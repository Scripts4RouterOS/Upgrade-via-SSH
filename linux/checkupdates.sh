#!/bin/bash
sdt=$(date '+%d/%m/%Y %H:%M:%S');
echo "Running update checks on all routerboards!" > checkupdates.txt
echo "Date and time:" >> checkupdates.txt
echo "$sdt" >> checkupdates.txt
echo "" >> checkupdates.txt

#Client 1
echo "Client 1" >> checkupdates.txt
ssh -o "StrictHostKeyChecking no" admin@router1.sn.mynetname.net "/system identity print" >> checkupdates.txt
ssh -o "StrictHostKeyChecking no" admin@router1.sn.mynetname.net "/system package update check-for-updates" >> checkupdates.txt
echo "" >> checkupdates.txt

#Client 2
echo "Client 2" >> checkupdates.txt
ssh -o "StrictHostKeyChecking no" admin@router2.sn.mynetname.net "/system identity print" >> checkupdates.txt
ssh -o "StrictHostKeyChecking no" admin@router2.sn.mynetname.net "/system package update check-for-updates" >> checkupdates.txt
echo "" >> checkupdates.txt

#Client 3
echo "Client 3" >> checkupdates.txt
ssh -o "StrictHostKeyChecking no" admin@router3.sn.mynetname.net "/system identity print" >> checkupdates.txt
ssh -o "StrictHostKeyChecking no" admin@router3.sn.mynetname.net "/system package update check-for-updates" >> checkupdates.txt
echo "" >> checkupdates.txt

#Client 4
echo "Client 4" >> checkupdates.txt
ssh -o "StrictHostKeyChecking no" admin@router4.sn.mynetname.net "/system identity print" >> checkupdates.txt
ssh -o "StrictHostKeyChecking no" admin@router4.sn.mynetname.net "/system package update check-for-updates" >> checkupdates.txt
echo "" >> checkupdates.txt

fdt=$(date '+%d/%m/%Y %H:%M:%S');
echo "Completed update checks on all routerboards!" >> checkupdates.txt
echo "Date and time:" >> checkupdates.txt
echo "$fdt" >> checkupdates.txt
echo "" >> checkupdates.txth
