#!/bin/bash
sdt=$(date '+%d/%m/%Y %H:%M:%S');
echo "Running update checks on all routerboards!" > doupdates.txt
echo "Date and time:" >> doupdates.txt
echo "$sdt" >> doupdates.txt
echo "" >> doupdates.txt

#Client 1
echo "Client 1" >> doupdates.txt
ssh -o "StrictHostKeyChecking no" admin@router1.sn.mynetname.net "/system identity print" >> doupdates.txt
ssh -o "StrictHostKeyChecking no" admin@router1.sn.mynetname.net "/system package update check-for-updates" >> doupdates.txt
echo "Upgrading in progess..." >> doupdates.txt
ssh -o "StrictHostKeyChecking no" admin@router1.sn.mynetname.net "/system package update download" >> doupdates.txt
ssh -o "StrictHostKeyChecking no" admin@router1.sn.mynetname.net "/interface bridge enable brReboot"
rdt=$(date '+%d/%m/%Y %H:%M:%S');
echo "Completed upgrade and reboot queue for 2am..." >> doupdates.txt
echo "Date and time:" >> doupdates.txt
echo "$rdt" >> doupdates.txt
echo "" >> doupdates.txt

#Client 2
echo "Client 2" >> doupdates.txt
ssh -o "StrictHostKeyChecking no" admin@router2.sn.mynetname.net "/system identity print" >> doupdates.txt
ssh -o "StrictHostKeyChecking no" admin@router2.sn.mynetname.net "/system package update check-for-updates" >> doupdates.txt
echo "Upgrading in progess..." >> doupdates.txt
ssh -o "StrictHostKeyChecking no" admin@router2.sn.mynetname.net "/system package update download" >> doupdates.txt
ssh -o "StrictHostKeyChecking no" admin@router2.sn.mynetname.net "/interface bridge enable brReboot"
rdt=$(date '+%d/%m/%Y %H:%M:%S');
echo "Completed upgrade and reboot queue for 2am..." >> doupdates.txt
echo "Date and time:" >> doupdates.txt
echo "$rdt" >> doupdates.txt
echo "" >> doupdates.txt

#Client 3
echo "Client 3" >> doupdates.txt
ssh -o "StrictHostKeyChecking no" admin@router3.sn.mynetname.net "/system identity print" >> doupdates.txt
ssh -o "StrictHostKeyChecking no" admin@router3.sn.mynetname.net "/system package update check-for-updates" >> doupdates.txt
echo "Upgrading in progess..." >> doupdates.txt
ssh -o "StrictHostKeyChecking no" admin@router3.sn.mynetname.net "/system package update download" >> doupdates.txt
ssh -o "StrictHostKeyChecking no" admin@router3.sn.mynetname.net "/interface bridge enable brReboot"
rdt=$(date '+%d/%m/%Y %H:%M:%S');
echo "Completed upgrade and reboot queue for 2am..." >> doupdates.txt
echo "Date and time:" >> doupdates.txt
echo "$rdt" >> doupdates.txt
echo "" >> doupdates.txt

#Client 4
echo "Client 4" >> doupdates.txt
ssh -o "StrictHostKeyChecking no" admin@router4.sn.mynetname.net "/system identity print" >> doupdates.txt
ssh -o "StrictHostKeyChecking no" admin@router4.sn.mynetname.net "/system package update check-for-updates" >> doupdates.txt
echo "Upgrading in progess..." >> doupdates.txt
ssh -o "StrictHostKeyChecking no" admin@router4.sn.mynetname.net "/system package update download" >> doupdates.txt
ssh -o "StrictHostKeyChecking no" admin@router4.sn.mynetname.net "/interface bridge enable brReboot"
rdt=$(date '+%d/%m/%Y %H:%M:%S');
echo "Completed upgrade and reboot queue for 2am..." >> doupdates.txt
echo "Date and time:" >> doupdates.txt
echo "$rdt" >> doupdates.txt
echo "" >> doupdates.txt

fdt=$(date '+%d/%m/%Y %H:%M:%S');
echo "Completed update checks on all routerboards!" >> doupdates.txt
echo "Date and time:" >> doupdates.txt
echo "$fdt" >> doupdates.txt
echo "" >> doupdates.txt


