# Upgrade-via-SSH

When you have a lot of RBs to maintain updates can be tedious. Auto-update? Sometimes you want to wait and make sure there are no serious issues. Here are a few scripts I put together to run the process on all RBs at once.

Step 1:

Create a SSH Key pair on your PC/Device. (If you do not have SSH installed, there are alot of how to documents to be found via your favourite search engine, but let's be real, if you are managing RBs chances are you are already using SSH for something else.)

Linux:
> $ ssh-keygen -t rsa -b 4096

The key will be placed in $HOME/.ssh

You will find two files of importance. id_rsa (WHICH YOU NEVER SHARE WITH ANYONE) and id_rsa.pub (which is your public key).

Connect to your RB with Winbox:

1. Upload the file id_rsa.pub
>    FILES > UPLOAD

2. Install the public key
>    SYSTEM > USERS > SSH KEYS > IMPORT SSH KEY

    You can then select the file id_rsa.pub and enter the username of the account you administer.

    You should now see the User listed followed by your username@yourdevicename

3. Ensure SSH is enabled
>    IP > SERVICES > SSH

    Once in SSH, ensure it is enabled, and if you have a static IP address, this is where you can enter it, to add an extra layer of security.

4. Test it out
    From your Linux Terminal...

>    $ ssh user@rbaddress

    You should get a lovely welcome screen that looks like:

>      MMMM    MMMM       KKK                          TTTTTTTTTTT      KKK
>      MMM MMMM MMM  III  KKK  KKK  RRRRRR     OOOOOO      TTT     III  KKK  KKK
>      MMM  MM  MMM  III  KKKKK     RRR  RRR  OOO  OOO     TTT     III  KKKKK
>      MMM      MMM  III  KKK KKK   RRRRRR    OOO  OOO     TTT     III  KKK KKK
>      MMM      MMM  III  KKK  KKK  RRR  RRR   OOOOOO      TTT     III  KKK  KKK
>    
>      MikroTik RouterOS 6.49.6 (c) 1999-2022       http://www.mikrotik.com/
>    [?]             Gives the list of available commands
>    command [?]     Gives help on the command and list of arguments
>    [Tab]           Completes the command/word. If the input is ambiguous,
>                    a second [Tab] gives possible options  
>    /               Move up to base level
>    ..              Move up one level
>    /command        Use command at the base level
>    [admin@MyFavRouter]

    You can type quit and press enter to exit back to your device.

5. Now for the scripting.
    The issue I had, was that updates require a reboot, however, you can hold that reboot off for the early hours of the morning when the client is not using their network. The time I chose was what suited by scenario, choose a time that is best for you.

    checkupdates.sh
    This I put in a cron to run daily. This allows me to have a checkupdates.txt file which I can open and check if updates are needed. I don't run the updates there and then, I watch the Mikrotik forums to see if anyone is reporting any issues. Remember to make the file executable using chmod +x checkupdates.sh (the text file is overwritten each time the script is run)

    Duplicate the paragraphs for each and every routerboard. I will eventually get round to making a script that just reads the RB address and username out of a list file.

    doupdates.sh
    This one I execute manually. Once I am comfortable that I am not going to end up with dozens of angry clients on the phone, I execute this script. It will output a file doupdates.txt with all the results. Again, the file is overwritten each time.

    Duplicate the paragraphs for each and every routerboard. I will eventually get round to making a script that just reads the RB address and username out of a list file.

    rebootwatcher.scr
    This you want to run on each and every routerboard. When doupdates.sh is run, it connects to each routerboard, downloads the latest version, and then instead of just rebooting, it sets a flag with a useless bridge interface (it serves no other purpose than for this task) and then a netwatch will check and if the bridge interface is enabled, it will reboot at a predetermined time, reseting all the flags bag to their waiting mode to ensure no unrequired reboots.

    I do plan on automating more of this for new routerboards as soon as I have some time.

Feel free to expand on this project. Maybe a version of Windows and macOS?



Created by Ze'ev
